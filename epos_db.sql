-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2017 at 05:39 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `epos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`category_id`, `category_name`) VALUES
(1, 'COFFEE'),
(3, 'NON COFFEE'),
(4, 'ICED TEA'),
(5, 'FRAPPE'),
(6, 'MILKTEA'),
(7, 'SNACKS'),
(8, 'SANDWICHES'),
(9, 'PASTA'),
(10, 'NANANANA');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discounts`
--

CREATE TABLE IF NOT EXISTS `tbl_discounts` (
  `id` int(11) NOT NULL,
  `discountName` varchar(50) NOT NULL,
  `discountAmount` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discounts`
--

INSERT INTO `tbl_discounts` (`id`, `discountName`, `discountAmount`) VALUES
(1, 'SENIOR', '20'),
(3, 'STUDENT', '5');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE IF NOT EXISTS `tbl_orders` (
  `o_id` int(11) NOT NULL,
  `orderName` varchar(100) NOT NULL,
  `orderPrice` varchar(100) NOT NULL,
  `orderSize` varchar(100) NOT NULL,
  `orderCategory` varchar(100) NOT NULL,
  `orderSubcategory` varchar(100) NOT NULL,
  `orderQuantity` varchar(100) NOT NULL,
  `orderTotal` int(11) NOT NULL,
  `transactionNumber` varchar(100) NOT NULL,
  `orderTag` varchar(10) NOT NULL,
  `cashierName` varchar(100) NOT NULL,
  `orderDate` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` (`o_id`, `orderName`, `orderPrice`, `orderSize`, `orderCategory`, `orderSubcategory`, `orderQuantity`, `orderTotal`, `transactionNumber`, `orderTag`, `cashierName`, `orderDate`) VALUES
(1, 'Java', '50', ' SMALL ', 'COFFEE', 'HOT', '10', 450, '000055', '', 'Mike Santos', '03/29/2017'),
(2, 'BURFRIES', '90', '  ', 'SNACKS', '', '1', 58, '000057', '', 'Mike Santos', '03/29/2017'),
(3, 'CLASSIC', '10', ' S ', 'COFFEE', 'HOT', '1', 23, '000057', '', 'Mike Santos', '03/29/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pricelist`
--

CREATE TABLE IF NOT EXISTS `tbl_pricelist` (
  `pid` int(11) NOT NULL,
  `price` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `pproductName` varchar(100) NOT NULL,
  `pproductCategory` varchar(100) NOT NULL,
  `pproductSubcategory` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pricelist`
--

INSERT INTO `tbl_pricelist` (`pid`, `price`, `size`, `pproductName`, `pproductCategory`, `pproductSubcategory`) VALUES
(2, '50', 'SMALL', 'CLASSIC', 'COFFEE', 'HOT'),
(3, '50', 'SMALL', 'Java', 'COFFEE', 'HOT'),
(4, '90', '', 'BURFRIES', 'SNACKS', ''),
(2, '10', 'S', 'CLASSIC', 'COFFEE', 'HOT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE IF NOT EXISTS `tbl_products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_description` varchar(200) NOT NULL,
  `product_category` varchar(100) NOT NULL,
  `product_subcategory` varchar(100) NOT NULL,
  `product_tags` varchar(200) NOT NULL,
  `product_status` varchar(100) NOT NULL,
  `promo_status` varchar(15) NOT NULL,
  `promo_amount` float NOT NULL,
  `paths` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `product_name`, `product_description`, `product_category`, `product_subcategory`, `product_tags`, `product_status`, `promo_status`, `promo_amount`, `paths`) VALUES
(2, 'CLASSIC', ' asasas', 'COFFEE', 'HOT', 'asasas', 'AVAILABLE', 'active', 0, '../assets/images/uploaded_files/products/default.png'),
(3, 'Java', '  This is java ', 'COFFEE', 'HOT', 'bbbbbb', 'AVAILABLE', 'inactive', 0.1, '../assets/images/uploaded_files/products/coffee hot.jpg'),
(4, 'BURFRIES', '  Burger na with fries', 'SNACKS', '', 'BURGER FRIES BUNDLE', 'AVAILABLE', 'active', 0.2, '../assets/images/uploaded_files/products/1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ratings`
--

CREATE TABLE IF NOT EXISTS `tbl_ratings` (
  `id` int(11) NOT NULL,
  `cashierName` varchar(100) NOT NULL,
  `rating` varchar(50000) NOT NULL,
  `rateBy` varchar(100) NOT NULL,
  `transactionNumber` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ratings`
--

INSERT INTO `tbl_ratings` (`id`, `cashierName`, `rating`, `rateBy`, `transactionNumber`) VALUES
(1, 'Mike Santos', '5', 'Komi', '0'),
(2, 'Mike Santos', '5', 'Maxie', '000015'),
(3, 'DINEIN', '3', 'Arjay perreras', '000057'),
(4, 'Mike Santos', '3', 'Justin', '000055');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subcategories`
--

CREATE TABLE IF NOT EXISTS `tbl_subcategories` (
  `subcategory_id` int(11) NOT NULL,
  `subcategory_name` varchar(100) NOT NULL,
  `subcategory_status` varchar(100) NOT NULL,
  `subcategory_main` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subcategories`
--

INSERT INTO `tbl_subcategories` (`subcategory_id`, `subcategory_name`, `subcategory_status`, `subcategory_main`) VALUES
(1, 'HOT', 'AVAILABLE', 'COFFEE'),
(2, 'ICED', 'AVAILABLE', 'COFFEE'),
(3, 'HOT', 'AVAILABLE', 'NON COFFEE'),
(4, 'ICED', 'AVAILABLE', 'NON COFFEE'),
(5, 'COLD', 'AVAILABLE', 'COFFEE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temporders`
--

CREATE TABLE IF NOT EXISTS `tbl_temporders` (
  `id` int(11) NOT NULL,
  `orderName` varchar(100) NOT NULL,
  `orderSize` varchar(100) NOT NULL,
  `orderPrice` varchar(100) NOT NULL,
  `orderQuantity` varchar(100) NOT NULL,
  `orderTotal` varchar(100) NOT NULL,
  `orderCategory` varchar(100) NOT NULL,
  `orderSubcategory` varchar(100) NOT NULL,
  `cashierName` varchar(100) NOT NULL,
  `transactionNumber` varchar(100) NOT NULL,
  `orderTag` varchar(10) NOT NULL,
  `orderDate` varchar(100) NOT NULL,
  `checkBox` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_transaction` (
  `tid` int(11) NOT NULL,
  `customerName` varchar(100) NOT NULL,
  `totalItems` varchar(100) NOT NULL,
  `subTotal` varchar(100) NOT NULL,
  `customerDiscount` varchar(100) NOT NULL,
  `totalPrice` varchar(100) NOT NULL,
  `customerCash` varchar(100) NOT NULL,
  `customerChange` varchar(100) NOT NULL,
  `transactionNumber` varchar(100) NOT NULL,
  `orderTag` varchar(10) NOT NULL,
  `cashierName` varchar(100) NOT NULL,
  `orderDate` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`tid`, `customerName`, `totalItems`, `subTotal`, `customerDiscount`, `totalPrice`, `customerCash`, `customerChange`, `transactionNumber`, `orderTag`, `cashierName`, `orderDate`) VALUES
(1, 'Jumar', '10', '500', '', '450', '1000', '550', '000055', 'DINEIN', 'Mike Santos', '03/29/2017'),
(2, 'xxx', '2', '100', 'SENIOR', '80.6', '85', '4.4', '000057', 'DINEIN', 'Mike Santos', '03/29/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactionnumbers`
--

CREATE TABLE IF NOT EXISTS `tbl_transactionnumbers` (
  `id` int(11) NOT NULL,
  `transactionNumber` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transactionnumbers`
--

INSERT INTO `tbl_transactionnumbers` (`id`, `transactionNumber`) VALUES
(1, '000000'),
(2, '000001'),
(3, '000002'),
(4, '000003'),
(5, '000004'),
(6, '000005'),
(7, '000006'),
(8, '000007'),
(9, '000008'),
(10, '000009'),
(11, '000010'),
(12, '000011'),
(13, '000012'),
(14, '000013'),
(15, '000014'),
(16, '000015'),
(17, '000016'),
(18, '000017'),
(19, '000018'),
(20, '000019'),
(21, '000020'),
(22, '000021'),
(23, '000022'),
(24, '000023'),
(25, '000024'),
(26, '000025'),
(27, '000026'),
(28, '000027'),
(29, '000028'),
(30, '000029'),
(31, '000030'),
(32, '000031'),
(33, '000032'),
(34, '000033'),
(35, '000034'),
(36, '000035'),
(37, '000036'),
(38, '000037'),
(39, '000038'),
(40, '000039'),
(41, '000040'),
(42, '000041'),
(43, '000042'),
(44, '000043'),
(45, '000044'),
(46, '000045'),
(47, '000046'),
(48, '000047'),
(49, '000048'),
(50, '000049'),
(51, '000050'),
(52, '000051'),
(53, '000052'),
(54, '000053'),
(55, '000054'),
(56, '000055'),
(57, '000056'),
(58, '000057'),
(59, '000058'),
(60, '000059'),
(61, '000060'),
(62, '000061');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(100) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `password_temp` text NOT NULL,
  `firstName` text NOT NULL,
  `middleName` text NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `userlevel` text NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `password_temp`, `firstName`, `middleName`, `lastName`, `address`, `userlevel`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'Jus', 'Ayson', 'Amd', 'a@y.c', 'ADMIN', 'NO'),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 'Mike', 'Luna', 'Santos', 'None', 'CASHIER', 'NO'),
(9, 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'superadmin', 'Justin Louise', 'Vinluan', 'Ayson', '09304874710', 'SUPERADMIN', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_discounts`
--
ALTER TABLE `tbl_discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_ratings`
--
ALTER TABLE `tbl_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subcategories`
--
ALTER TABLE `tbl_subcategories`
  ADD PRIMARY KEY (`subcategory_id`);

--
-- Indexes for table `tbl_temporders`
--
ALTER TABLE `tbl_temporders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `tbl_transactionnumbers`
--
ALTER TABLE `tbl_transactionnumbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_discounts`
--
ALTER TABLE `tbl_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_ratings`
--
ALTER TABLE `tbl_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_subcategories`
--
ALTER TABLE `tbl_subcategories`
  MODIFY `subcategory_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_temporders`
--
ALTER TABLE `tbl_temporders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_transactionnumbers`
--
ALTER TABLE `tbl_transactionnumbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
