<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico in the root directory -->


        <link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap.min.css">

        <!-- FOFNTAWESOME-->
        <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

    

        
    </head>
        

<style type="text/css">
    body{
    background-color: #444;   
}

.vertical-offset-100{
    padding-top:100px;
}
</style>

<body>

    <div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please sign in</h3>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" method="post" action="login_validation.php">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Username" name="uname" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="pword" type="password">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                            </label>
                        </div>
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


        
</body>
</html>
