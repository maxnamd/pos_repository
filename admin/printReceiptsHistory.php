<?php
date_default_timezone_set('Asia/Manila');
session_start();
if(($_SESSION['id']) && ($_SESSION['userlevel'])==="ADMIN"){
        $aid=$_SESSION['id'];
        $firstName=$_SESSION['firstName'];
        $lastName=$_SESSION['lastName'];
        $userlevel=$_SESSION['userlevel'];
        $apassword=$_SESSION['password'];
        $username=$_SESSION['username'];
    }
else{
        header("location:../index.php");
    }
?>
<?php

    include '../connection.php';

    $customerName = $_POST['customerName'];
	$totalItems = $_POST['totalItems'];
	$subTotal = $_POST['subTotal'];
	$customerDiscount = $_POST['customerDiscount'];
	$totalPrice = $_POST['totalPrice'];
	$customerCash = $_POST['customerCash'];
	$customerChange = $_POST['customerChange'];
	$transactionNumber = $_POST['transactionNumber'];
	$cashierName = $_POST['cashierName'];
	$orderDate = $_POST['orderDate'];			
    $currentDate = date('F j, Y');
?>


<html>
<head>
<title>RECEIPT</title>
	<link rel="stylesheet" href="../src/css/print-preview.css" type="text/css" media="screen">
    <script src="../assets/jquery/jquery-3.1.1.min.js"></script>
    <script src="../assets/printjquery/jquery.print-preview.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(function() {
            /*
             * Initialise example carousel
             */
            $("#feature > div").scrollable({interval: 2000}).autoscroll();
            
            /*
             * Initialise print preview plugin
             */
            // Add link for print preview and intialise
            $('#aside').prepend('<a class="print-preview">Print this page</a>');
            $('a.print-preview').printPreview();
            
            // Add keybinding (not recommended for production use)
            $(document).bind('keydown', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 80 && !$('#print-modal').length) {
                    $.printPreview.loadPrintPreview();
                    return false;
	                }            
            });
        });
    </script>


    <script type="text/javascript">
    	window.onload = function() { window.print(); }
    </script>


</head>

<body>
	<div style="width: 50% !important">
	<table style='font-family:Segoe UI Light;font-size:12pt;'>
		<tr>
			    <center><img id="logo" src="../assets/images/main/logo.png" alt="CH Logo" style='margin-right: 20px;height: 80px;width:80px;'></center> 
				<td height ='150'>
			    <center style='padding-left:50px;padding-bottom: 30px;'>
				CAFFEINE HUB <br>
				LINGAYEN<br>
				Contact No. (075)09028 82787
                </center>    
			</td>
		</tr>
 	</table>

    
 	<table style='padding-top:90px;'>

                       
        <!--<?php echo "$cashierName"; ?>--><p>Cahier:&nbsp;&nbsp;<?php echo "$username"?></p>
        	<p> Date: <?php echo $orderDate;?></p>
        	
        	<p> Transaction Number: <?php echo $transactionNumber;?></p>
        <p>-------------------------------------------------------------------</p>
 		<th> Qty.</th>

 		<th> Item</th>
 		<th style='padding-left:50px;'>&nbsp;&nbsp;Price</th>
 		<th> </th>
		<?php
		$sum = 0;
		$query="SELECT * from tbl_orders where transactionNumber = '$transactionNumber'";
		$result=mysql_query($query);
		while (($row =mysql_fetch_array($result))) {
		
		?>
		<thead>
				
		</thead>	

			<tr>

                

				<td> <?php echo $row['orderQuantity']; ?> </td>
				<td> <?php echo $row['orderName']; ?> </td>
				<td style='margin-right: 20px;'> <?php echo $row['orderPrice']; ?> </td>
				<td> <?php echo $row['orderTotal']; ?> </td>
				
			</tr>
			

		<?php }
			

		?>


		<tr >
			<td> </td>
			<td> </td>
			<td style='padding-top:50px;'> Subtotal: </td>
			<td style='padding-top:50px;'> 
				<?php 
					echo "$subTotal";
				?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> Discount: </td>
			<td> 
				<?php echo $customerDiscount;?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> TOTAL: </td>
			<td>
				<?php 
				echo "$totalPrice";
				
				
				?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> Cash: </td>
			<td>
				<?php echo $customerCash?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> Change: </td>
			<td>
				<?php echo $customerChange;?>
			</td>

		</tr>
           

	</table>
	<p style='font-style:Times New Roman;font-size:30px;margin-left:50px;'>THANK YOU !!!</p>
	<p style='font-style:Times New Roman;font-size:15px;margin-left:50px;'>DATE PRINTED: <?php echo $currentDate;?></p>


	</div>

</body>
</html>