<?php
date_default_timezone_set('Asia/Manila');
session_start();
if(($_SESSION['id']) && ($_SESSION['userlevel'])==="CASHIER"){
        $aid=$_SESSION['id'];
        $firstName=$_SESSION['firstName'];
        $lastName=$_SESSION['lastName'];
        $userlevel=$_SESSION['userlevel'];
        $apassword=$_SESSION['password'];
        $username=$_SESSION['username'];
    }
else{
        header("location:../index.php");
    }
?>
<?php

    include '../connection.php';


    $sesDiscount = $_SESSION['hiddenDiscount'];
    $sesCash = $_SESSION['customerCash'];
    $sesSubtotal = $_SESSION['subTotal'];

    $cashierName = $firstName . " " . $lastName;

    $currentDate = date('F j, Y');
?>


<html>
<head>
<title>RECEIPT</title>
	<link rel="stylesheet" href="../src/css/print-preview.css" type="text/css" media="screen">
    <script src="../assets/jquery/jquery-3.1.1.min.js"></script>
    <script src="../assets/printjquery/jquery.print-preview.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(function() {
            /*
             * Initialise example carousel
             */
            $("#feature > div").scrollable({interval: 2000}).autoscroll();
            
            /*
             * Initialise print preview plugin
             */
            // Add link for print preview and intialise
            $('#aside').prepend('<a class="print-preview">Print this page</a>');
            $('a.print-preview').printPreview();
            
            // Add keybinding (not recommended for production use)
            $(document).bind('keydown', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 80 && !$('#print-modal').length) {
                    $.printPreview.loadPrintPreview();
                    return false;
	                }            
            });
        });
    </script>


    <script type="text/javascript">
    	window.onload = function() { window.print(); }
    </script>


</head>

<body>
<div style="width: 50% !important">
	<table style='font-family:Segoe UI Light;font-size:12pt;'>
		<tr>
			    <center><img id="logo" src="../assets/images/main/logo.png" alt="CH Logo" style='height: 160px;width:200px;'></center> 
				<td height ='150'>
			    <center style='padding-left:50px;padding-bottom: 30px;'>
				CAFFEINE HUB <br>
				LINGAYEN<br>
				Contact No. (075)09028 82787
                </center>    
			</td>
		</tr>
 	</table>

    
 	<table style='padding-top:90px;'>

		<p>Cahier:&nbsp;&nbsp;<?php echo "$cashierName"?></p>
        	<p> <?php echo $currentDate;?></p>
        	<p> Transaction Number: <?php echo $_SESSION['transactionNumber'];;?></p>

        <p>-------------------------------------------------------------------</p>
 		<th> Qty.</th>

 		<th> </th>
 		<th style='padding-left:50px;'>&nbsp;&nbsp;Price</th>
 		<th> </th>
		<?php
		$sum = 0;
		$query="SELECT * FROM tbl_temporders WHERE cashierName = '$cashierName'";
		$result=mysql_query($query);
		while (($row =mysql_fetch_array($result))) {
		
		?>
		<thead>
				
		</thead>	

			<tr>

                

				<td> <?php echo $row['orderQuantity']; ?> </td>
				<td> <?php echo $row['orderName']; ?> </td>
				<td style='margin-right: 20px;'> <?php echo $row['orderPrice']; ?> </td>
				<td> <?php echo $row['orderTotal']; ?> </td>
				
			</tr>
			

		<?php }
			

		?>


		<tr >
			<td> </td>
			<td> </td>
			<td style='padding-top:50px;'> Subtotal: </td>
			<td style='padding-top:50px;'> 
				<?php 

					echo $sesSubtotal;
				?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> Discount: </td>
			<td> 
				<?php 
				if (empty($_SESSION['ses_selectedname'])) {
					# code...
					echo "NO DISCOUNT";
				}
				else
				{	
				echo $_SESSION['ses_selectedname'];
				}

				?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> TOTAL: </td>
			<td>

				<?php 
					$add = mysql_query("SELECT SUM(orderTotal) FROM tbl_temporders WHERE orderTotal >= 1;")or die(mysql_error());
					list ( $rsvp_total ) = mysql_fetch_array($add); 

				?>

				<?php 
				if ($sesDiscount == "0%") {
					# code...
					$result = ($rsvp_total);
					echo $result;
				}
				else
				{
					$result = ($rsvp_total) - ( floatval($rsvp_total) *(floatval($sesDiscount) / 100.0) );
					echo $result;
				}
				mysql_query("DELETE from tbl_temporders WHERE cashierName='$cashierName'") or die(mysql_error());
		        echo "<script>
		            bootbox.alert('Success! Order taken!', 
		                function() {
		                setTimeout('window.location.replace(\'dashboard.php\')',600);
		                });
   
		            </script>";
				?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> Cash: </td>
			<td>
				<?php echo $sesCash?>
			</td>

		</tr>

		<tr>
			<td> </td>
			<td> </td>
			<td> Change: </td>
			<td>
				<?php echo $sesCash -  $result;?>
			</td>

		</tr>
           

	</table>
	<p style='font-style:Times New Roman;font-size:30px;margin-left:50px;'>THANK YOU !!!</p>



</div>
</body>
</html>